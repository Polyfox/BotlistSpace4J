package eu.polyfox.botlistspace.impl;

import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.entities.Server;
import de.btobastian.javacord.listener.server.ServerJoinListener;
import de.btobastian.javacord.listener.server.ServerLeaveListener;
import eu.polyfox.botlistspace.BotlistSpace4J;
import eu.polyfox.botlistspace.BotlistSpaceAPI;

/**
 * Automatically update bot stats on botlist.space.
 * @author tr808axm
 */
public class JavacordBotlistSpaceUpdater implements ServerJoinListener, ServerLeaveListener {
    private final BotlistSpaceAPI api;

    /**
     * Constructs a listener that automatically updates the botlist.space stats whenever the bot joins or leaves a guild.
     * @param token the token that authorizes this application to update the bot's stats on botlist.space. You can
     *              get a token for your bot on the website by clicking "Token" on your bot's page (you need to be
     *              logged in for that).
     * @see #subscribe(String, DiscordAPI) the subscribe(String,DiscordAPI) method automatically registers the listener
     */
    public JavacordBotlistSpaceUpdater(String token) {
        api = BotlistSpace4J.createClient(token);
    }

    /**
     * Subscribes a listener to your JDA instance that automatically updates the botlist.space stats whenever the bot
     * joins or leaves a guild.
     * @param token the token that authorizes this application to update the bot's stats on botlist.space. You can
     *              get a token for your bot on the website by clicking "Token" on your bot's page (you need to be
     *              logged in for that).
     * @param discordAPI the Javacord API instance to subscribe to.
     */
    public static void subscribe(String token, DiscordAPI discordAPI) {
        discordAPI.registerListener(new JavacordBotlistSpaceUpdater(token));
    }

    /**
     * Updates the botlist.space stats when the bot joins (is added to) a guild.
     */
    @Override
    public void onServerJoin(DiscordAPI discordAPI, Server server) {
        updateStats(discordAPI);
    }

    /**
     * Updates the botlist.space stats when the bot leaves a guild.
     */
    @Override
    public void onServerLeave(DiscordAPI discordAPI, Server server) {
        updateStats(discordAPI);
    }

    /**
     * Method used to avoid code duplication. Updates the stats on botlist.space based on the provided Discord API client.
     * Should only be called by the event listeners.
     */
    private void updateStats(DiscordAPI discordAPI) {
        api.updateStats(discordAPI.getYourself().getId(), discordAPI.getServers().size());
    }
}

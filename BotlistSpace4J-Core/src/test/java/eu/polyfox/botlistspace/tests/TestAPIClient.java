package eu.polyfox.botlistspace.tests;

import eu.polyfox.botlistspace.BotlistSpace4J;
import eu.polyfox.botlistspace.BotlistSpaceAPI;
import eu.polyfox.botlistspace.entities.Bot;
import eu.polyfox.botlistspace.entities.SiteStats;
import eu.polyfox.botlistspace.entities.User;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Tests whether the API client is created correctly.
 * @author tr808axm
 */
public class TestAPIClient {
    @Test
    public void testAPICreation() {
        BotlistSpaceAPI api = BotlistSpace4J.createClient();
        Assert.assertNotNull(api);
    }

    /**
     * Get a sample bot and check if standard information is included.
     */
    @Test
    public void testGetBot() {
        Bot bot = BotlistSpace4J.createClient().getBot("380713705073147915");
        checkPopulated(bot);
    }

    /**
     * Get all bots and check whether the list is populated.
     */
    @Test
    public void testGetAllBots() {
        Bot[] allBots = BotlistSpace4J.createClient().getAllBots();
        Assert.assertNotNull(allBots);
        Assert.assertTrue(allBots.length > 0);
        for (Bot bot : allBots)
            checkPopulated(bot);
    }

    private void checkPopulated(Bot bot) {
        Assert.assertNotNull(bot);
        // Assert.assertNotNull(bot.getAppID()); some bots don't have an appID entry
        // Assert.assertNotNull(bot.getAvatar()); some bots don't have an avatar entry
        Assert.assertNotNull(bot.getDiscriminator());
        Assert.assertNotNull(bot.getId());
        Assert.assertNotNull(bot.getInvite());
        // Assert.assertNotNull(bot.getLibrary()); some bots don't have a library entry
        Assert.assertNotNull(bot.isApproved() + " bot " + bot.getId() + " has no name", bot.getUsername());
        Assert.assertNotNull(bot.getOwners());
        Assert.assertFalse(bot.getOwners().length == 0);
        for (User owner : bot.getOwners())
            checkPopulated(owner, false);
        Assert.assertNotNull(bot.getPrefix());
        Assert.assertNotNull(bot.getShortDesc());
        Assert.assertTrue(bot.getTimestamp() > 0);
        Assert.assertTrue(bot.getViews() > 0);
    }

    @Test
    public void testUpdateStats() {
        Properties testProperties = getTestProperties("bot-id", "token", "server-count");
        Assume.assumeNotNull(testProperties);

        String token = testProperties.getProperty("token");
        String botId = testProperties.getProperty("bot-id");
        int serverCount = Integer.valueOf(testProperties.getProperty("server-count"));
        BotlistSpaceAPI api = BotlistSpace4J.createClient(token);
        api.updateStats(botId, serverCount);
    }

    @Test
    public void testUpdateStatsSharded() {
        Properties testProperties = getTestProperties("bot-id", "token", "server-count");
        Assume.assumeNotNull(testProperties);

        String token = testProperties.getProperty("token");
        String botId = testProperties.getProperty("bot-id");
        int serverCount = Integer.valueOf(testProperties.getProperty("server-count"));
        BotlistSpaceAPI api = BotlistSpace4J.createClient(token);
        api.updateStats(botId, new int[]{serverCount});
    }

    @Test
    public void testGetUser() {
        User user = BotlistSpace4J.createClient().getUser("149505704569339904");
        checkPopulated(user, true);
    }

    private void checkPopulated(User user, boolean checkBots) {
        // Assert.assertNotNull(bot.getOwners()[0].getAvatar()); some users don't have an avatar entry
        Assert.assertNotNull(user.getDiscriminator());
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getUsername());
        if (checkBots) {
            Assert.assertNotNull(user.getBots());
            for (Bot bot : user.getBots())
                checkPopulated(bot);
        }
    }

    @Test
    public void testGetUpvoteUsers() {
        Properties testProperties = getTestProperties("bot-id", "token");
        Assume.assumeNotNull(testProperties);

        String token = testProperties.getProperty("token");
        String botId = testProperties.getProperty("bot-id");
        BotlistSpaceAPI api = BotlistSpace4J.createClient(token);
        User[] upvoteUsers = api.getUpvoteUsers(botId);
        for (User user : upvoteUsers)
            checkPopulated(user, false); // maybe turn this to true? can't test
    }

    @Test
    public void testGetUpvoteIds() {
        Properties testProperties = getTestProperties("bot-id", "token");
        Assume.assumeNotNull(testProperties);

        String token = testProperties.getProperty("token");
        String botId = testProperties.getProperty("bot-id");
        BotlistSpaceAPI api = BotlistSpace4J.createClient(token);
        User[] upvoteUsers = api.getUpvoteUsers(botId);
        String[] upvoteIds = api.getUpvoteIds(botId);
        for (String upvoteId : upvoteIds)
            Assert.assertNotNull(upvoteId);
        // check same result
        Assert.assertEquals(upvoteUsers.length, upvoteIds.length);
        List<String> upvoteIdList = Arrays.asList(upvoteIds);
        for (User user : upvoteUsers)
            Assert.assertTrue(upvoteIdList.contains(user.getId()));
    }

    @Test
    public void testGetSiteStats() {
        SiteStats stats = BotlistSpace4J.createClient().getSiteStats();
        Assert.assertNotNull(stats);
        Assert.assertTrue(stats.getApprovedBots() > 0);
    }

    /**
     * Tries to read test data.
     * @return test data or null if it was not found.
     */
    private Properties getTestProperties(String... requiredKeys) {
        try {
            File testDataFile = new File("test-data.properties");
            if (testDataFile.exists()) {
                Properties properties = new Properties();
                properties.load(new FileInputStream(testDataFile));
                boolean containsAllKeys = true;
                for (int i = 0; i < requiredKeys.length && containsAllKeys; i++)
                    if (!properties.containsKey(requiredKeys[i]))
                        containsAllKeys = false;
                if (containsAllKeys)
                    return properties;
            }
        } catch (Throwable ignored) {}
        return null;
    }
}

package eu.polyfox.botlistspace.retrofit;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Automatically executes the call Retrofit creates and returns the response body.
 * Kind of copied from
 * <a href="https://github.com/DiscordBotList/DBL-Java-Library/blob/master/src/main/java/org/discordbots/api/client/retrofit/GenericCallAdapterFactory.java">
 *     DBL's Java library</a> but there really is no other short way of implementing this.
 * @author tr808axm
 */
public class CallExecutionAdapterFactory extends CallAdapter.Factory {
    /**
     * I actually don't know why this should be useful in any way. Why don't you just use the constructor?
     * @return a new instance of this Factory.
     */
    public static CallExecutionAdapterFactory create() {
        return new CallExecutionAdapterFactory();
    }

    @Override
    public CallAdapter<?, ?> get(final Type type, Annotation[] annotations, Retrofit retrofit) {
        return new CallAdapter<Object, Object>() {
            @Override
            public Type responseType() {
                return type;
            }

            @Override
            public Object adapt(Call<Object> call) {
                try {
                    return call.execute().body();
                } catch (Exception e) {
                    System.out.println("An exception occurred while executing request to " + call.request().url());
                    return null;
                }
            }
        };
    }
}

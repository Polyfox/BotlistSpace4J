package eu.polyfox.botlistspace.entities;

import lombok.Getter;

/**
 * JSON representation of a Bot on botlist.space.
 * @author tr808axm
 */
@Getter
public class Bot {
    /**
     * The discord application id of the bot.
     */
    private String appID;
    /**
     * Whether the bot is approved by the botlist.space staff.
     */
    private boolean approved;
    /**
     * The full avatar URL (not only the hash any more).
     */
    private String avatar;
    /**
     * The bot-user's discriminator (the 4-digit thing behind the number sign).
     * @see <a href="https://discordapp.com/developers/docs/resources/user#user-object">Discord user object documentation</a>
     */
    private String discriminator;
    /**
     * Whether the bot is featured on the home page of botlist.space.
     */
    private boolean featured;
    /**
     * The bot's user id.
     * @see <a href="https://discordapp.com/developers/docs/resources/user#user-object">Discord user object documentation</a>
     */
    private String id;
    /**
     * The oauth2 invite link for the bot.
     * @see <a href="https://discordapp.com/developers/docs/topics/oauth2">Discord oauth2 authorization</a>
     */
    private String invite;
    /**
     * The discord API wrapper used for the bot.
     * <a href="https://discordapi.com/unofficial/libs.html">Unofficial list of discord API libraries</a>
     */
    private String library;
    /**
     * The bot owners' botlist.space user objects.
     */
    private User[] owners;
    /**
     * The prefix which the bot reacts to in chat.
     */
    private String prefix;
    /**
     * Whether the bot is premium on botlist.space.
     */
    private boolean premium;
    /**
     * Short bot description without formatting (up to 120 characters).
     */
    private String shortDesc;
    /**
     * When the bot added to the botlist.space website.
     */
    private long timestamp;
    /**
     * The bot's user name.
     * @see <a href="https://discordapp.com/developers/docs/resources/user#user-object">Discord user object documentation</a>
     */
    private String username;
    /**
     * Extension for a vanity url, e.g. {@code "vexera"} for {@code https://botlist.space/view/vexera}.
     * Only exists for premium bots.
     * @see #premium
     */
    private String vanity;
    /**
     * How often the bot's botlist.space page has been viewed.
     */
    private int views;

    /**
     * Old property naming.
     * @return the bot's user name.
     * @deprecated since Apr 6th. Use {@link #getUsername()} instead.
     */
    @Deprecated
    public String getName() {
        return username;
    }
}

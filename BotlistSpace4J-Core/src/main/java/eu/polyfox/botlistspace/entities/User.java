package eu.polyfox.botlistspace.entities;

import lombok.Getter;

/**
 * JSON representation of a User on botlist.space.
 * @author tr808axm
 */
@Getter
public class User {
    /**
     * The full avatar URL (not only the hash any more).
     */
    private String avatar;
    /**
     * All bots that the user has added to botlist.space. May be null if this object was fetched using {@link eu.polyfox.botlistspace.BotlistSpaceAPI#getBot(String)}.
     */
    private Bot[] bots;
    /**
     * The user's discriminator (the 4-digit thing behind the number sign).
     * @see <a href="https://discordapp.com/developers/docs/resources/user#user-object">Discord user object documentation</a>
     */
    private String discriminator;
    /**
     * The user's id.
     * @see <a href="https://discordapp.com/developers/docs/resources/user#user-object">Discord user object documentation</a>
     */
    private String id;
    /**
     * The user's name.
     * @see <a href="https://discordapp.com/developers/docs/resources/user#user-object">Discord user object documentation</a>
     */
    private String username;
}

package eu.polyfox.botlistspace.entities;

import lombok.Getter;

/**
 * JSON representation of a the botlist.space stats.
 * @author tr808axm
 */
@Getter
public class SiteStats {
    /**
     * The number of approved bots on botlist.space.
     */
    private int approvedBots;
    /**
     * The number of unapproved bots on botlist.space.
     */
    private int unapprovedBots;
}

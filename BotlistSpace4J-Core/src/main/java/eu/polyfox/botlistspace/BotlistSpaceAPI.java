package eu.polyfox.botlistspace;

import eu.polyfox.botlistspace.entities.Bot;
import eu.polyfox.botlistspace.entities.SiteStats;
import eu.polyfox.botlistspace.entities.User;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

import java.util.HashMap;
import java.util.Map;

/**
 * Java wrapper for the botlist.space API.
 * @author tr808axm
 */
public interface BotlistSpaceAPI {
    /**
     * From the <a href="https://botlist.space/docs/api">official documentation</a>:
     * <p>
     * Represents <a href="https://botlist.space/docs/api#get-api-stats">{@code GET /api/stats}</a>.
     * Returns basic information about the site.
     * </p>
     * @return a SiteStats object.
     */
    @GET("stats")
    SiteStats getSiteStats();

    /**
     * From the <a href="https://botlist.space/docs/api">official documentation</a>:
     * <p>
     * Represents <a href="https://botlist.space/docs/api#get-api-bots">{@code GET /api/bots}</a>.
     * Gets a list of all of the bots on the website.
     * </p>
     * @return an array of bots.
     */
    @GET("bots")
    Bot[] getAllBots();

    /**
     * From the <a href="https://botlist.space/docs/api">official documentation</a>:
     * <p>
     * Represents <a href="https://botlist.space/docs/api#get-api-bots-id">{@code GET /api/bots/:id}</a>.
     * Gets information on a specific bot based on ID.
     * </p>
     * @return a Bot object.
     */
    @GET("bots/{id}")
    Bot getBot(@Path("id") String botId);

    /**
     * From the <a href="https://botlist.space/docs/api">official documentation</a>:
     * <p>
     * Represents <a href="https://botlist.space/docs/api#post-api-bots-id-auth-required-">{@code POST /api/bots/:id}</a>.
     * Submit your bot's server count to the site.
     * </p>
     */
    @POST("bots/{id}")
    Void updateStats(@Path("id") String botId, @Body Map<String, Object> statsBody);

    /**
     * From the <a href="https://botlist.space/docs/api">official documentation</a>:
     * <p>
     * Represents <a href="https://botlist.space/docs/api#post-api-bots-id-auth-required-">{@code POST /api/bots/:id}</a>.
     * Submit your bot's server count to the site using a single server count.
     * </p>
     */
    default void updateStats(String botId, int serverCount) {
        Map<String, Object> body = new HashMap<>();
        body.put("server_count", serverCount);
        updateStats(botId, body);
    }

    /**
     * From the <a href="https://botlist.space/docs/api">official documentation</a>:
     * <p>
     * Represents <a href="https://botlist.space/docs/api#post-api-bots-id-auth-required-">{@code POST /api/bots/:id}</a>.
     * Submit your bot's server count to the site using per-shard server counts.
     * </p>
     */
    default void updateStats(String botId, int[] shardCounts) {
        Map<String, Object> body = new HashMap<>();
        body.put("shards", shardCounts);
        updateStats(botId, body);
    }

    /**
     * From the <a href="https://botlist.space/docs/api">official documentation</a>:
     * <p>
     * Represents <a href="https://botlist.space/docs/api#get-api-bots-id-upvotes-auth-required-">{@code GET /api/bots/:id/upvotes}</a>.
     * Gets all of the upvotes for a bot. Requires the bot to have premium to see this data.
     * </p>
     * @return an array of the users who upvoted.
     */
    @GET("bots/{id}/upvotes")
    User[] getUpvoteUsers(@Path("id") String botId);

    /**
     * From the <a href="https://botlist.space/docs/api">official documentation</a>:
     * <p>
     * Represents <a href="https://botlist.space/docs/api#get-api-bots-id-upvotes-auth-required-">{@code GET /api/bots/:id/upvotes?ids=true}</a>.
     * Gets all of the upvotes for a bot. Requires the bot to have premium to see this data.
     * </p>
     * @return an array of ids from the users who upvoted.
     */
    @GET("bots/{id}/upvotes?ids=true")
    String[] getUpvoteIds(@Path("id") String botId);

    /**
     * From the <a href="https://botlist.space/docs/api">official documentation</a>:
     * <p>
     * Represents <a href="https://botlist.space/docs/api#get-api-users-id">{@code GET /api/users/:id}</a>.
     * Gets the user's listed bots on the site.
     * </p>
     * @return a User object.
     */
    @GET("users/{id}")
    User getUser(@Path("id") String userId);
}

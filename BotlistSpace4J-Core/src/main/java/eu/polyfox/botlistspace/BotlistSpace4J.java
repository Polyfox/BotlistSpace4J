package eu.polyfox.botlistspace;

import eu.polyfox.botlistspace.retrofit.CallExecutionAdapterFactory;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Constructor class for the botlist.space REST API wrapper.
 * @author tr808axm
 */
public class BotlistSpace4J {
    /**
     * Constructs a new API client without authorization.
     * @return an API client.
     */
    public static BotlistSpaceAPI createClient() {
        return createClient(null);
    }

    /**
     * Constructs a new API client.
     * @param token needed for authorization. Authorization is only needed for specific actions which can be reviewed
     *              at the <a href="https://botlist.space/docs/api">API's documentation site</a>.
     * @return an API client.
     */
    public static BotlistSpaceAPI createClient(String token) {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        // Request JSON
        clientBuilder.addInterceptor((Interceptor.Chain chain) -> {
            Request.Builder reqBuilder = chain.request().newBuilder();
            if (token != null)
                reqBuilder.addHeader("Authorization", token);
            reqBuilder.addHeader("Content-Type", "application/json");
            return chain.proceed(reqBuilder.build());
        });
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://botlist.space/api/")
                .addCallAdapterFactory(CallExecutionAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientBuilder.build())
                .build();
        return retrofit.create(BotlistSpaceAPI.class);
    }
}

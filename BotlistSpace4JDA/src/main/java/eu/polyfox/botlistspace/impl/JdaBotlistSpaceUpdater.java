package eu.polyfox.botlistspace.impl;

import eu.polyfox.botlistspace.BotlistSpace4J;
import eu.polyfox.botlistspace.BotlistSpaceAPI;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

/**
 * Automatically update bot stats on botlist.space.
 * @author tr808axm
 */
public class JdaBotlistSpaceUpdater extends ListenerAdapter {
    private final BotlistSpaceAPI api;

    /**
     * Constructs a listener that automatically updates the botlist.space stats whenever the bot joins or leaves a guild.
     * @param token the token that authorizes this application to update the bot's stats on botlist.space. You can
     *              get a token for your bot on the website by clicking "Token" on your bot's page (you need to be
     *              logged in for that).
     * @see #subscribe(String, JDA) the subscribe(String,JDA) method automatically registers the listener
     */
    public JdaBotlistSpaceUpdater(String token) {
        api = BotlistSpace4J.createClient(token);
    }

    /**
     * Subscribes a listener to your JDA instance that automatically updates the botlist.space stats whenever the bot
     * joins or leaves a guild.
     * @param token the token that authorizes this application to update the bot's stats on botlist.space. You can
     *              get a token for your bot on the website by clicking "Token" on your bot's page (you need to be
     *              logged in for that).
     * @param jda the JDA instance to subscribe to.
     */
    public static void subscribe(String token, JDA jda) {
        jda.addEventListener(new JdaBotlistSpaceUpdater(token));
    }

    /**
     * Updates the botlist.space stats when the bot joins (is added to) a guild.
     */
    @Override
    public void onGuildJoin(GuildJoinEvent event) {
        updateStats(event.getJDA());
    }

    /**
     * Updates the botlist.space stats when the bot leaves a guild.
     */
    @Override
    public void onGuildLeave(GuildLeaveEvent event) {
        updateStats(event.getJDA());
    }

    /**
     * Method used to avoid code duplication. Updates the stats on botlist.space based on the provided JDA. Should only
     * be called by the event listeners.
     */
    private void updateStats(JDA jda) {
        api.updateStats(jda.getSelfUser().getId(), jda.getGuilds().size());
    }
}

[![][jitpack-version]][jitpack-repo]

# BotlistSpace4J
A Java API wrapper for the [botlist.space](https://botlist.space/) REST API.

## Using BotlistSpace4J
### with Discord4J
Keeping your Discord4J-bot's botlist.space stats up-to-date is as easy as adding the following to your client initialization by adding the `BotlistSpace4Discord4J` module :

```java
IDiscordClient client = ...; // keep your old client-initialization process
Discord4JBotlistSpaceUpdater.subscribe("your botlist.space token", client);
```

### with Javacord
Keeping your Javacord-bot's botlist.space stats up-to-date is as easy as adding the following to your client initialization by adding the `BotlistSpace4Javacord` module :

```java
DiscordAPI apiClient = ...; // keep your old client-initialization process
JavacordBotlistSpaceUpdater.subscribe("your botlist.space token", apiClient);
```

### with JDA
Keeping your JDA-bot's botlist.space stats up-to-date is as easy as adding the following to your client initialization by adding the `BotlistSpace4JDA` module :

```java
JDA jda = ...; // keep your old client-initialization process
JdaBotlistSpaceUpdater.subscribe("your botlist.space token", jda);
```

### with Other libraries / do-it-yourself
You can implement the stats-posting yourself using the `BotlistSpace4J-Core` module:

```java
BotlistSpaceAPI api = BotlistSpace4J.createClient("your botlist.space token");
String botId = ...; // The discord ID of your bot        
int serverCount = ...; // the amount of guilds your bot is on
api.updateStats(botId, serverCount);
```

## Adding it to your project
### Static (JAR)
Download the latest version on the [tags page](https://gitlab.com/Polyfox/BotlistSpace4J/tags).
### Maven
You can add it as a Maven dependency using [Jitpack](https://jitpack.io/#com.gitlab.Polyfox/BotlistSpace4J).
1.  Add the Jitpack repository to your POM-file:

    ```xml
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
    ```
2.  Add the BotlistSpace4J repository to your POM-file and replace `VERSION` with the latest version ([![][jitpack-version]][jitpack-repo]) or `master-SNAPSHOT` for the development build. Also replace `MODULE`with the module you want to import (see [Using BotlistSpace4J](#using-botlistspace4j) for information on what module to use).

    ```xml
    <dependency>
        <groupId>eu.polyfox.BotlistSpace4J</groupId>
        <artifactId>MODULE</artifactId>
        <version>VERSION</version>
    </dependency>
    ```
### Other build tools
Jitpack also supports gradle, sbt and leiningen. Go check out the guide on [their page][jitpack-repo].

[jitpack-version]: https://jitpack.io/v/com.gitlab.Polyfox/BotlistSpace4J.svg
[jitpack-repo]: https://jitpack.io/#com.gitlab.Polyfox/BotlistSpace4J
package eu.polyfox.botlistspace.impl;

import eu.polyfox.botlistspace.BotlistSpace4J;
import eu.polyfox.botlistspace.BotlistSpaceAPI;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.guild.GuildCreateEvent;
import sx.blah.discord.handle.impl.events.guild.GuildLeaveEvent;

/**
 * Automatically update bot stats on botlist.space.
 * @author tr808axm
 */
public class Discord4JBotlistSpaceUpdater {
    private final BotlistSpaceAPI api;
    private boolean clientReady = false;

    /**
     * Constructs a listener that automatically updates the botlist.space stats whenever the bot joins or leaves a guild.
     * @param token the token that authorizes this application to update the bot's stats on botlist.space. You can
     *              get a token for your bot on the website by clicking "Token" on your bot's page (you need to be
     *              logged in for that).
     * @see #subscribe(String, IDiscordClient) the subscribe(String,IDiscordClient) method automatically registers the listener
     */
    public Discord4JBotlistSpaceUpdater(String token) {
        api = BotlistSpace4J.createClient(token);
    }

    /**
     * Subscribes a listener to your IDiscordClient instance that automatically updates the botlist.space stats whenever the bot
     * joins or leaves a guild.
     * @param token the token that authorizes this application to update the bot's stats on botlist.space. You can
     *              get a token for your bot on the website by clicking "Token" on your bot's page (you need to be
     *              logged in for that).
     * @param discordClient the Discord4J client instance to subscribe to.
     */
    public static void subscribe(String token, IDiscordClient discordClient) {
        discordClient.getDispatcher().registerListener(new Discord4JBotlistSpaceUpdater(token));
    }


    /**
     * Updates the botlist.space stats when the bot joins (is added to) a guild. (Mass calls from the startup process
     * are ignored)
     */
    @EventSubscriber
    public void onGuildCreate(GuildCreateEvent event) {
        if (clientReady)
            updateStats(event.getClient());
    }

    /**
     * Enables updating in the {@link #onGuildCreate(GuildCreateEvent)} listener to suppress mass event calls on bot
     * startup.
     */
    @EventSubscriber
    public void onReady(ReadyEvent event) {
        clientReady = true;
    }

    /**
     * Updates the botlist.space stats when the bot leaves a guild.
     */
    @EventSubscriber
    public void onGuildLeave(GuildLeaveEvent event) {
        updateStats(event.getClient());
    }

    /**
     * Method used to avoid code duplication. Updates the stats on botlist.space based on the provided Discord client.
     * Should only be called by the event listeners.
     */
    private void updateStats(IDiscordClient client) {
        api.updateStats(client.getApplicationClientID(), client.getGuilds().size());
    }
}
